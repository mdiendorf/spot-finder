# spot-finder Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .


## Package and running serverless application

### JVM
Build and Package application using:
```shell script
./mvnw clean package
```

Create or update function on AWS (change lambda role arn!):
```shell script
LAMBDA_ROLE_ARN="arn:aws:iam::1234567890:role/lambda-ex" ./target/manage create|update|delete
```

Example for running local lambda with given payload
```shell script
sam local invoke --template target/sam.jvm.yaml --event payload.json
```

### Native
Build and Package application using:
```shell script
./mvnw clean package -Dnative
```

Create or update function on AWS (change lambda role arn!):
```shell script
LAMBDA_ROLE_ARN="arn:aws:iam::1234567890:role/lambda-ex" ./target/manage native create|update|delete
```

Example for running local lambda with given payload
```shell script
sam local invoke --template target/sam.native.yaml --event payload.json
```

### Payload example
```json
{
  "mesh_json_path": "assets/mesh_x_sin_cos_10000.json",
  "num_view_spots": 5
}
```

The following mesh files are available
<ul>
    <li>assets/mesh.json</li>
    <li>assets/mesh_x_sin_cos_10000.json</li>
    <li>assets/mesh_x_sin_cos_20000.json</li>
</ul>