package lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import domain.dto.ViewSpotDto;
import domain.service.MeshParser;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;

@Named("lambda")
public class LambdaMain implements RequestHandler<ParseMeshRequest, List<ViewSpotDto>> {

    @Inject
    MeshParser meshParser;

    @Override
    public List<ViewSpotDto> handleRequest(ParseMeshRequest input, Context context) {
        try {
            meshParser.init(input.getMeshJsonPath());
            return meshParser.findViewSpots(input.getNumViewSpots());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
