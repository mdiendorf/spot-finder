package lambda;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParseMeshRequest {

    @JsonProperty("mesh_json_path")
    private String meshJsonPath;

    @JsonProperty("num_view_spots")
    private int numViewSpots;
}
