package domain.service;

import domain.dto.ViewSpotDto;
import domain.json.ElementValue;
import domain.json.MeshRoot;
import domain.json.NodeVertex;
import io.quarkus.logging.Log;

import javax.enterprise.context.Dependent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


@Dependent
public class ViewSpotFinder {
	private Iterator<ElementValue> orderedElementIterator;

	private Set<NodeVertex> vertexCache;


	/**
	 * Identify the view spots for a walking tour, starting from the highest
	 * view spot and following the next best neighbor strategy
	 * @param mesh The mesh (coming from json deserialization)
	 * @param n how much view spots should be found
	 * @return A list of {@link ViewSpotDto} objects
	 */
	public List<ElementValue> findViewSpots(MeshRoot mesh, int n) {
		if (mesh == null)
			throw new IllegalArgumentException("Mesh parameter can't be null");

		List<ElementValue> viewSpots = new ArrayList<>();

		vertexCache = new HashSet<>();
		orderedElementIterator = mesh.getValues().iterator();

		// start element is the highest local maxima
		ElementValue elementValue = orderedElementIterator.next();
		if (elementValue != null) {
			// search N view spots
			for (int i = 0; i < n; i++) {
				cacheVertices(elementValue);

				viewSpots.add(elementValue);
				elementValue = findNextLocalMaxElementValue(elementValue, orderedElementIterator.next());

				if (elementValue == null) {
					Log.error("No further local maxima available, exiting...");
					break;
				}
			}
		}

		return viewSpots;
	}


	/**
	 * Find the next best neighbor (local maxima) starting from current position of
	 * the ordered list
	 * @param forElementValue find the neighbor for which element
	 * @param iteratorElementVal iterator of all elements ordered by spot height
	 * @return next neighbor
	 */
	private ElementValue findNextLocalMaxElementValue(ElementValue forElementValue, ElementValue iteratorElementVal) {
		if (forElementValue == null || iteratorElementVal == null) {
			return null;
		}

		// check if any vertex has already been processed
		if (iteratorElementVal.getElement().getVertices().stream()
				.anyMatch(nodeVertex -> vertexCache.contains(nodeVertex))) {

			if (orderedElementIterator.hasNext()) {
				// not a local maxima, recursive call now with next element in iterator to check if it is a local maxima
				return findNextLocalMaxElementValue(forElementValue, orderedElementIterator.next());
			}
		} else {

			// next element is not a neighbor --> it's a local maxima
			return iteratorElementVal;
		}

		return null;
	}

	private void cacheVertices(ElementValue elementValue) {
		vertexCache.addAll(elementValue.getElement().getVertices());
	}
}
