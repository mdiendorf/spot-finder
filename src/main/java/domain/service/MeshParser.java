package domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.dto.ViewSpotDto;
import domain.json.ElementValue;
import domain.json.MeshRoot;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class MeshParser {

	@Inject
	ObjectMapper objectMapper;

	@Inject
	ViewSpotFinder viewSpotFinder;

	private MeshRoot meshRoot;

	public void init(String meshJsonFilePath) throws IOException {
		File file = new File(meshJsonFilePath);

		try (InputStream inputStream = new FileInputStream(file)) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
				String contents = reader.lines().collect(Collectors.joining(System.lineSeparator()));

				// Deserialize json to object model
				meshRoot = createMesh(contents);
			}
		}
	}

	public List<ViewSpotDto> findViewSpots(int numViewSpots) {
		// Find specified count of view spots
		List<ElementValue> result = viewSpotFinder.findViewSpots(meshRoot, numViewSpots);

		// Map element values to view spot dtos
		return result.stream().map(ViewSpotDto::convert).collect(Collectors.toList());
	}

	/**
	 * Create the mesh object based on given json input
	 * The list of values will also be sorted (desc)
	 *
	 * @param inputJson json content
	 * @return a {@link MeshRoot} instance
	 * @throws JsonProcessingException error on json deserialization
	 */
	private MeshRoot createMesh(String inputJson) throws JsonProcessingException {
		MeshRoot meshRoot = objectMapper.readValue(inputJson, MeshRoot.class);

		meshRoot.setValues(meshRoot.getValues().stream()
				.sorted((o1, o2) -> Double.compare(o2.getSpotHeightAvg(), o1.getSpotHeightAvg()))
				.collect(Collectors.toCollection(LinkedHashSet::new)));

		return meshRoot;
	}


}
