package domain.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.logging.Log;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class JsonUtil {

	@Inject
	ObjectMapper objectMapper;

	/**
	 * Pretty prints every object in json format
	 * @param object any java object
	 * @throws JsonProcessingException error on json serialization
	 */
	public void prettyPrint(Object object) throws JsonProcessingException {
		Log.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object));
	}
}
