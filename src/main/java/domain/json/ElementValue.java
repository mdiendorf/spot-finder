package domain.json;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@EqualsAndHashCode(of = "element")
@JsonIdentityInfo(property = "element_id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Element.class)
public class ElementValue {
	@JsonProperty("element_id")
	private Element element;

	@JsonProperty("value")
	private double spotHeightAvg;
}
