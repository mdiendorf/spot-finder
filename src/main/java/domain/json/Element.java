package domain.json;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter @Setter
@EqualsAndHashCode(of = "id")
@ToString(of = {"id", "vertices"})
@JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class, scope = Element.class)
public class Element {
	private long id;

	@JsonProperty("nodes")
	private List<NodeVertex> vertices;

}
