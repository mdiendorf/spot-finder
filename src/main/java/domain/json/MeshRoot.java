package domain.json;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Set;

@Data
public class MeshRoot {

	@JsonProperty("nodes")
	private Set<NodeVertex> nodeVertices;
	private Set<Element> elements;
	private Set<ElementValue> values;
}
