package domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import domain.json.ElementValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ViewSpotDto {
	@JsonProperty("element_id")
	private long elementId;

	@JsonProperty("value")
	private double height;

	public static ViewSpotDto convert(ElementValue elementValue) {
		return new ViewSpotDto(elementValue.getElement().getId(), elementValue.getSpotHeightAvg());
	}
}
