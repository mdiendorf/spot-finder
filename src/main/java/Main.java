import domain.dto.ViewSpotDto;
import domain.service.MeshParser;
import domain.util.JsonUtil;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

@QuarkusMain
public class Main {

	public static void main(String... args) {
		Quarkus.run(SpotFinderApp.class, args);
	}


	public static class SpotFinderApp implements QuarkusApplication {

		@Inject
		MeshParser meshParser;

		@Inject
		JsonUtil jsonUtil;


		/**
		 * Main method:
		 * Given a mesh and an integer number N, find the first N view spots ordered by the spot height starting from the highest to the lowest.
		 *
		 * @param args
		 * 1st argument: The absolut path to a JSON file with the mesh and the height values. The file contains three sections:
		 * nodes, elements, and values. The syntax is as follows:
		 * {
		 * 	nodes: [
		 * 		{id: node_id1, x: <number value>, y: <number value>},
		 * 		{id: node_id2, x: <number value>, y: <number value>},
		 * 		{id: node_id3, x: <number value>, y: <number value>},
		 * 		...
		 * 	],
		 * 	elements: [
		 * 		{id: element_id1, nodes: [node_id1, node_id2, node_id3]},
		 * 		...
		 * 	],
		 * 	values: [
		 * 		{element_id: element_id1, value: <number value>},
		 * 		...
		 * 	]
		 * }
		 *
		 * 2nd argument: Integer number N that defines how many view spots must be found and written out.
		 * @return A JSON file with the mesh and the height values. The file contains three sections:
		 * nodes, elements, and values. The syntax is as follows:
		 * {
		 * 	nodes: [
		 * 		{id: node_id1, x: <number value>, y: <number value>},
		 * 		{id: node_id2, x: <number value>, y: <number value>},
		 * 		{id: node_id3, x: <number value>, y: <number value>},
		 * 		...
		 * 	],
		 * 	elements: [
		 * 		{id: element_id1, nodes: [node_id1, node_id2, node_id3]},
		 * 		...
		 * 	],
		 * 	values: [
		 * 		{element_id: element_id1, value: <number value>},
		 * 		...
		 * 	]
		 * }
		 * @throws IOException file reading error
		 */
		@Override
		public int run(String[] args) throws IOException {
			if (args.length >= 1) {
				String filePath = args[0];
				int numViewSpots = Integer.parseInt(args[1]);


				meshParser.init(filePath);
				List<ViewSpotDto> resultDtos = meshParser.findViewSpots(numViewSpots);
				jsonUtil.prettyPrint(resultDtos);
			}

			return 0;
		}
	}
}
